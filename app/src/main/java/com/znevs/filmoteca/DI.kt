package com.znevs.filmoteca

import com.znevs.core.network.di.NetworkComponent
import com.znevs.filmoteca.di.AppComponent

object DI {
  lateinit var appComponent: AppComponent
    internal set

  lateinit var networkComponent: NetworkComponent
    internal set
}