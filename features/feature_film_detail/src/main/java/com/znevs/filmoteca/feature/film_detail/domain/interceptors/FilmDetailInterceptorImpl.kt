package com.znevs.filmoteca.feature.film_detail.domain.interceptors

import com.znevs.core.network.error.DataEmptyError
import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.filmoteca.feature.film_detail.domain.model.FilmDetailDomainModel
import com.znevs.filmoteca.feature.film_detail.domain.repository.FilmDetailRepository
import javax.inject.Inject

class FilmDetailInterceptorImpl @Inject constructor(
    private val repository: FilmDetailRepository
) : FilmDetailInterceptor {

    override suspend fun loadFilmDetail(filmId: Long): ResultWrapper<FilmDetailDomainModel> {
        when (val result = repository.loadFilmDetail(filmId)) {
            is ResultWrapper.Success -> {
                val data = result.value
                return if (data != null) {
                    ResultWrapper.Success(
                        FilmDetailDomainModel(
                            filmId = data.filmId,
                            description = data.description ?: "",
                            nameEn = data.nameEn ?: "",
                            nameRu = data.nameRu ?: "",
                            posterUrl = data.posterUrl ?: "",
                            year = data.year ?: ""
                        )
                    )
                } else {
                    ResultWrapper.Error(DataEmptyError)
                }
            }
            is ResultWrapper.Error -> {
                return result
            }
        }
    }
}