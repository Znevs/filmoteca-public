package com.znevs.core.network.di

import com.znevs.core.network.BuildConfig
import com.znevs.core.network.api.ApiService
import com.znevs.core.network.api.interceptors.HeaderInterceptor
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Component(modules = [NetworkModule::class])
@Singleton
interface NetworkComponent {

  fun api(): ApiService

}

@Module
abstract class NetworkModule {

  companion object {
    private const val BASE_URL = "https://api.rawg.io/"

    @Provides
    @Singleton
    fun provideApi(): ApiService  = Retrofit.Builder()
      .addConverterFactory(MoshiConverterFactory.create())
      .baseUrl(BuildConfig.API_URL)
      .client(
        OkHttpClient.Builder()
          .addInterceptor(HeaderInterceptor())
          .addInterceptor(HttpLoggingInterceptor().apply {
            level =
              if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
              else HttpLoggingInterceptor.Level.NONE
          })
          .build()
      )
      .build()
      .create(ApiService::class.java)
  }
}