package com.znevs.core.network.api.interceptors

import com.znevs.core.network.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("X-API-KEY", BuildConfig.API_KEY)
                .build()
        )
    }
}