package com.znevs.filmoteca

import android.app.Application
import com.znevs.core.network.di.DaggerNetworkComponent
import com.znevs.filmoteca.di.DaggerAppComponent
import com.znevs.filmoteca.util.ResourceProvider
import timber.log.Timber
import javax.inject.Inject

class App : Application() {

    @Inject
    lateinit var resourceProvider: ResourceProvider

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        initDI()
    }

    private fun initDI() {
        DI.appComponent = DaggerAppComponent.builder()
            .appContext(this)
            .build()

        DI.networkComponent = DaggerNetworkComponent.create()
    }
}