package com.znevs.filmoteca.domain.model.main

import com.znevs.core.domain.model.base.ListItem

data class FilmItem(
    val id: Long,
    val title: String,
    val image: String?
) : ListItem {
    override val itemId: Long = id
}