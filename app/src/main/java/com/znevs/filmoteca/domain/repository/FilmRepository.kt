package com.znevs.filmoteca.domain.repository

import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.filmoteca.domain.model.main.FilmItem

interface FilmRepository {
    suspend fun loadTopFilms(page: Int): ResultWrapper<List<FilmItem>>
    suspend fun loadLatestReleaseFilms(page: Int): ResultWrapper<List<FilmItem>>
}