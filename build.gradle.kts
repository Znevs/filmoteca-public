buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.1.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.10")
        classpath(CoreDependency.SAFE_ARGS)

        classpath(gradleApi())
        classpath(localGroovy())
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

