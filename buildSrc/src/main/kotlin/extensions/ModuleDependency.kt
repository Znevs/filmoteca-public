package extensions

import kotlin.reflect.full.memberProperties

private const val BASE_PREFIX = ":base"
private const val FEATURE_PREFIX = ":features:feature_"

object ModuleDependency {
    const val APP = ":app"
    const val CORE = "${BASE_PREFIX}:core"
    const val CORE_NETWORK = "${BASE_PREFIX}:core_network"
    const val FILM_DETAIL = "${FEATURE_PREFIX}film_detail"

    fun getAllModules() = ModuleDependency::class.memberProperties
        .filter { it.isConst }
        .map { it.getter.call().toString() }
        .toSet()

    fun getDynamicFeatureModules() = getAllModules()
        .filter { it.startsWith(FEATURE_PREFIX) }
        .toSet()
}
