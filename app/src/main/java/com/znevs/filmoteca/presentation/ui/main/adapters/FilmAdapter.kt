package com.znevs.filmoteca.presentation.ui.main.adapters

import com.hannesdorfmann.adapterdelegates4.paging.PagedListDelegationAdapter
import com.znevs.core.domain.model.base.ListItem
import com.znevs.filmoteca.domain.model.main.FilmItem
import com.znevs.core.presentation.ui.base.BaseDiffUtilItemCallback

class FilmAdapter(itemClickedListener: (FilmItem) -> Unit) : PagedListDelegationAdapter<ListItem>(BaseDiffUtilItemCallback()) {

    init {
        delegatesManager.addDelegate(MainScreenDelegates.filmAdapterDelegate(itemClickedListener))
            .addDelegate(MainScreenDelegates.filmProgressAdapterDelegate())
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.itemId ?: 0L
    }
}