package com.znevs.filmoteca.data.repository

import android.util.Log
import com.znevs.core.network.api.base.safeApiCall
import com.znevs.core.network.api.ApiService
import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.filmoteca.domain.model.main.FilmItem
import com.znevs.filmoteca.domain.repository.FilmRepository
import javax.inject.Inject

class FilmRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : FilmRepository {

    override suspend fun loadTopFilms(page: Int): ResultWrapper<List<FilmItem>> {
        return safeApiCall {
            val response = apiService.getTopFilms(page)
            return@safeApiCall response?.films?.map {
                FilmItem(id = it.filmId, title = it.nameRu ?: "", image = it.posterUrl)
            } ?: emptyList()
        }
    }

    override suspend fun loadLatestReleaseFilms(page: Int): ResultWrapper<List<FilmItem>> {
        return safeApiCall {
            val response = apiService.getLatestReleasesFilms(page)
            return@safeApiCall response?.releases?.map {
                FilmItem(id = it.filmId, title = it.nameRu ?: "", image = it.posterUrl)
            } ?: emptyList()
        }
    }
}