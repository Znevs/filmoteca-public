package com.znevs.filmoteca.domain.interceptors

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.znevs.core.domain.model.base.ListItem

interface MainScreenInteractor {
    fun topFilms(): LiveData<PagedList<ListItem>>
    fun latestReleases(): LiveData<PagedList<ListItem>>
}