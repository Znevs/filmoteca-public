package com.znevs.core.network.api.response

data class TopFilmsResponse(
    val pagesCount: Int,
    val films: List<com.znevs.core.network.model.FilmDataModel>
)