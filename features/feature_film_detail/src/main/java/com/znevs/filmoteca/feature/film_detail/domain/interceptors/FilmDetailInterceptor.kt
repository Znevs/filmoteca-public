package com.znevs.filmoteca.feature.film_detail.domain.interceptors

import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.filmoteca.feature.film_detail.domain.model.FilmDetailDomainModel

interface FilmDetailInterceptor {
    suspend fun loadFilmDetail(filmId: Long): ResultWrapper<FilmDetailDomainModel>
}