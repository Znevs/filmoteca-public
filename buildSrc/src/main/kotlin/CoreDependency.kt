// Versions consts that are used across libraries and Gradle plugins
object CoreVersion {
    const val KOTLIN = "1.4.10"
    const val GRADLE = "4.0.0"
}

object CoreDependency {
    const val GRADLE = "com.android.tools.build:gradle:${CoreVersion.GRADLE}"
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${CoreVersion.KOTLIN}"
    const val SAFE_ARGS = "androidx.navigation:navigation-safe-args-gradle-plugin:2.3.0"
}