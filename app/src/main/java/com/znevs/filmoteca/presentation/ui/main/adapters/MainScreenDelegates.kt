package com.znevs.filmoteca.presentation.ui.main.adapters

import android.widget.ImageView
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import com.znevs.filmoteca.R
import com.znevs.core.domain.model.base.ListItem
import com.znevs.filmoteca.domain.model.main.FilmItem
import com.znevs.filmoteca.domain.model.main.FilmsHorizontalItem
import com.znevs.filmoteca.domain.model.main.ProgressFilmItem
import kotlinx.android.synthetic.main.item_film.*
import kotlinx.android.synthetic.main.item_films_horizontal.*


object MainScreenDelegates {
    fun filmHorizontalAdapterDelegate(itemClickedListener: (FilmItem) -> Unit) =
        adapterDelegateLayoutContainer<FilmsHorizontalItem, FilmsHorizontalItem>(R.layout.item_films_horizontal) {
            val adapter = FilmAdapter(itemClickedListener)
            itemFilmsRecyclerView.adapter = adapter

            bind {
                itemFilmsTitleTextView.text = item.title
                adapter.submitList(item.films)
            }
        }

    fun filmAdapterDelegate(itemClickedListener: (FilmItem) -> Unit) =
        adapterDelegateLayoutContainer<FilmItem, ListItem>(R.layout.item_film) {
            bind {
                container.setOnClickListener { itemClickedListener(item) }
                itemFilmImageView.scaleType = ImageView.ScaleType.CENTER_CROP
                itemFilmImageView.load(item.image) {
                    crossfade(true)
                    placeholder(R.drawable.ic_launcher_foreground)
                    transformations(RoundedCornersTransformation(10f))
                }
                itemFilmTitleTextView.text = item.title
            }
        }

    fun filmProgressAdapterDelegate() = adapterDelegateLayoutContainer<ProgressFilmItem, ListItem>(R.layout.item_progress_film) {}
}
