package com.znevs.filmoteca.feature.film_detail.presentation.ui.detail

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.znevs.core.presentation.ui.base.BaseFragment
import com.znevs.filmoteca.feature.film_detail.R
import com.znevs.filmoteca.feature.film_detail.presentation.viewmodel.detail.FilmDetailViewModel
import com.znevs.filmoteca.presentation.ui.main.MainFragmentDirections
import kotlinx.android.synthetic.main.fragment_film_detail.*
import timber.log.Timber

class FilmDetailFragment : BaseFragment(R.layout.fragment_film_detail) {

    private val component by lazy { FilmDetailComponent.create() }
    private val viewModel by viewModels<FilmDetailViewModel> { component.viewModelFactory() }
    private val args: FilmDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureObservables()

        viewModel.setArgs(args.data)
    }

    private fun configureObservables() {
        viewModel.data.observe(viewLifecycleOwner, Observer {
            Timber.d("FilmID = ${it.filmId}")
            filmDetailImageView.scaleType = ImageView.ScaleType.CENTER_CROP
            filmDetailImageView.load(it.posterUrl) {
                crossfade(true)
                placeholder(com.znevs.filmoteca.R.drawable.ic_launcher_foreground)
                transformations(RoundedCornersTransformation(10f))
            }
            filmDetailYearTitleTextView.text = "${it.nameRu}(${it.year})"
            filmDetailDescriptionTextView.text = it.description
        })
    }
}