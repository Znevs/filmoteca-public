package com.znevs.filmoteca.domain.pagination.base

sealed class PagingState {
    object Loading : PagingState()
    object Success : PagingState()
    object Error : PagingState()
}