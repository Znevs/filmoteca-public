package com.znevs.filmoteca.domain.pagination.main

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.znevs.core.domain.model.base.ListItem
import com.znevs.filmoteca.domain.repository.FilmRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class LatestReleaseDataSourceFactory @Inject constructor(
    private val repository: FilmRepository
) : DataSource.Factory<Int, ListItem>() {

    val source = MutableLiveData<LatestReleaseDataSource>()

    override fun create(): DataSource<Int, ListItem> {
        val source = LatestReleaseDataSource(repository, CoroutineScope(Dispatchers.IO))
        this.source.postValue(source)
        return source
    }
}