package com.znevs.filmoteca.presentation.ui.main.adapters

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.znevs.filmoteca.domain.model.main.FilmItem
import com.znevs.filmoteca.domain.model.main.FilmsHorizontalItem
import com.znevs.core.presentation.ui.base.BaseDiffUtilItemCallback

class MainScreenAdapter(itemClickedListener: (FilmItem) -> Unit) :
    AsyncListDifferDelegationAdapter<FilmsHorizontalItem>(BaseDiffUtilItemCallback()) {
    init {
        delegatesManager.addDelegate(
            MainScreenDelegates.filmHorizontalAdapterDelegate(itemClickedListener)
        )
    }
}