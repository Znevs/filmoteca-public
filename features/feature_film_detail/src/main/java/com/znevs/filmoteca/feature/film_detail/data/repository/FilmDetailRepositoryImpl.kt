package com.znevs.filmoteca.feature.film_detail.data.repository

import com.znevs.core.network.api.ApiService
import com.znevs.core.network.api.base.safeApiCall
import com.znevs.core.network.model.FilmDetailDataModel
import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.filmoteca.feature.film_detail.domain.repository.FilmDetailRepository
import javax.inject.Inject

class FilmDetailRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : FilmDetailRepository {
    override suspend fun loadFilmDetail(filmId: Long): ResultWrapper<FilmDetailDataModel?> {
        return safeApiCall {
            val response = apiService.getFilmDetail(filmId)
            return@safeApiCall response?.data
        }
    }
}