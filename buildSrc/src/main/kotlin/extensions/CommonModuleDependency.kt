package extensions

import TestLibraryDependency
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.artifacts.dsl.DependencyHandler

/*
Define common dependencies, so they can be easily updated across feature modules
 */
fun DependencyHandler.addTestDependencies() {
    testImplementation(TestLibraryDependency.JUNIT)
    testImplementation(TestLibraryDependency.KOTLIN_COROUTINES)

    androidTestImplementation(TestLibraryDependency.ESPRESSO)
    androidTestImplementation(TestLibraryDependency.KAKAO)

    androidTestImplementation(TestLibraryDependency.ANDROID_X_TEST_RUNNER)
    androidTestImplementation(TestLibraryDependency.ANDROID_X_TEST_RULES)
    androidTestImplementation(TestLibraryDependency.ANDROID_X_TEST_CORE)
    androidTestImplementation(TestLibraryDependency.ANDROID_X_TEST_EXT)

    androidTestImplementation(TestLibraryDependency.MOCKITO_ANDROID)

    debugImplementation(TestLibraryDependency.FRAGMENT)
    debugImplementation(TestLibraryDependency.MOCKITO)
}

fun DependencyHandler.addCoreDependencies() {
    api(project(ModuleDependency.CORE))
    api(project(ModuleDependency.CORE_NETWORK))
}

fun DependencyHandler.addAppDependencies() {
    implementation(project(ModuleDependency.APP))
}

/*
 * These extensions mimic the extensions that are generated on the fly by Gradle.
 * They are used here to provide above dependency syntax that mimics Gradle Kotlin DSL
 * syntax in module\build.gradle.kts files.
 */
@Suppress("detekt.UnusedPrivateMember")
private fun DependencyHandler.implementation(dependencyNotation: Any): Dependency? =
    add("implementation", dependencyNotation)

@Suppress("detekt.UnusedPrivateMember")
private fun DependencyHandler.api(dependencyNotation: Any): Dependency? =
    add("api", dependencyNotation)

@Suppress("detekt.UnusedPrivateMember")
private fun DependencyHandler.kapt(dependencyNotation: Any): Dependency? =
    add("kapt", dependencyNotation)

private fun DependencyHandler.testImplementation(dependencyNotation: Any): Dependency? =
    add("testImplementation", dependencyNotation)

private fun DependencyHandler.androidTestImplementation(dependencyNotation: Any): Dependency? =
    add("androidTestImplementation", dependencyNotation)

private fun DependencyHandler.debugImplementation(dependencyNotation: Any): Dependency? =
    add("debugImplementation", dependencyNotation)

private fun DependencyHandler.project(
    path: String,
    configuration: String? = null
): ProjectDependency {
    val notation = if (configuration != null) {
        mapOf("path" to path, "configuration" to configuration)
    } else {
        mapOf("path" to path)
    }

    return uncheckedCast(project(notation))
}

@Suppress("unchecked_cast", "nothing_to_inline", "detekt.UnsafeCast")
private inline fun <T> uncheckedCast(obj: Any?): T = obj as T
