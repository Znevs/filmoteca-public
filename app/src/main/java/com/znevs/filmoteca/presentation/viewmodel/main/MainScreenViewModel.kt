package com.znevs.filmoteca.presentation.viewmodel.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.paging.PagedList
import com.znevs.filmoteca.R
import com.znevs.filmoteca.domain.interceptors.MainScreenInteractorImpl
import com.znevs.core.domain.model.base.ListItem
import com.znevs.filmoteca.domain.model.main.FilmsHorizontalItem
import com.znevs.filmoteca.domain.model.main.MainScreenDomainModel
import com.znevs.core.presentation.viewmodel.base.BaseViewModel
import com.znevs.filmoteca.domain.interceptors.MainScreenInteractor
import com.znevs.filmoteca.util.ResourceProvider
import javax.inject.Inject

class MainScreenViewModel @Inject constructor(
    private val interactor: MainScreenInteractor,
    private val resourceProvider: ResourceProvider
) : BaseViewModel() {

    private val _data = MediatorLiveData<MainScreenDomainModel>()
    val data: LiveData<MainScreenDomainModel> = _data

    private var topFilmList: PagedList<ListItem>? = null
    private var latestReleaseList: PagedList<ListItem>? = null

    init {
        _data.addSource(interactor.latestReleases()) {
            latestReleaseList = it
            updateData()
        }
        _data.addSource(interactor.topFilms()) {
            topFilmList = it
            updateData()
        }
    }

    private fun updateData() {
        _data.value = MainScreenDomainModel(
            latestReleasesFilms = FilmsHorizontalItem(resourceProvider.string(R.string.latest_releases), latestReleaseList),
            topFilms = FilmsHorizontalItem(resourceProvider.string(R.string.top_films), topFilmList)
        )
    }
}