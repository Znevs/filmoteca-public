package com.znevs.core.network.api.response

import com.znevs.core.network.model.FilmDataModel

data class LatestReleaseFilmsResponse(
    val page: Int,
    val total: Int,
    val releases: List<FilmDataModel>
)