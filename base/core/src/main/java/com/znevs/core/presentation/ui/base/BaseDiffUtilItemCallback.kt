package com.znevs.core.presentation.ui.base

import androidx.recyclerview.widget.DiffUtil
import com.znevs.core.domain.model.base.ListItem

open class BaseDiffUtilItemCallback<T : ListItem> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.itemId == newItem.itemId
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.equals(newItem)
    }
}