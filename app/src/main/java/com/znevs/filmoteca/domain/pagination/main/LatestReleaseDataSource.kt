package com.znevs.filmoteca.domain.pagination.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.core.domain.model.base.ListItem
import com.znevs.filmoteca.domain.pagination.base.PagingState
import com.znevs.filmoteca.domain.repository.FilmRepository
import kotlinx.coroutines.*
import timber.log.Timber


class LatestReleaseDataSource(
    private val repository: FilmRepository,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, ListItem>() {

    private var supervisorJob = SupervisorJob()
    private val pagingState = MutableLiveData<PagingState>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, ListItem>) {
        executeQuery(1) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ListItem>) {
        val page = params.key
        executeQuery(page) {
            callback.onResult(it, page + 1)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ListItem>) {
        val page = params.key
        executeQuery(page) {
            callback.onResult(it, page - 1)
        }
    }

    private fun executeQuery(page: Int, callback: (List<ListItem>) -> Unit) {
        pagingState.postValue(PagingState.Loading)
        scope.launch(getJobErrorHandler() + supervisorJob) {
            when (val response = repository.loadLatestReleaseFilms(page)) {
                is ResultWrapper.Success -> {
                    pagingState.postValue(PagingState.Success)
                    callback(response.value)
                }
                else -> {
                    pagingState.postValue(PagingState.Error)
                }
            }
        }
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Timber.e(TopFilmsDataSource::class.java.simpleName, "An error happened: $e")
        pagingState.postValue(PagingState.Error)
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun getNetworkState(): LiveData<PagingState> = pagingState
}