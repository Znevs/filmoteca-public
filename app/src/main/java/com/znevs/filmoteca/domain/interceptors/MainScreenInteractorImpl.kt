package com.znevs.filmoteca.domain.interceptors

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.znevs.filmoteca.domain.pagination.base.PagingState
import com.znevs.filmoteca.domain.pagination.main.LatestReleaseDataSourceFactory
import com.znevs.filmoteca.domain.pagination.main.TopFilmsDataSource
import com.znevs.filmoteca.domain.pagination.main.TopFilmsDataSourceFactory
import com.znevs.filmoteca.domain.repository.FilmRepository
import javax.inject.Inject

class MainScreenInteractorImpl @Inject constructor(
    private val repository: FilmRepository
) : MainScreenInteractor {

    private val topFilmsDataSource =
        TopFilmsDataSourceFactory(
            repository = repository
        )
    private val topFilms = LivePagedListBuilder(topFilmsDataSource, pagedListConfig()).build()
    private val topFilmsPagingState: LiveData<PagingState>? = Transformations.switchMap(topFilmsDataSource.source) { it.getNetworkState() }

    private val latestReleaseDataSource =
        LatestReleaseDataSourceFactory(
            repository = repository
        )
    private val latestReleaseFilms = LivePagedListBuilder(latestReleaseDataSource, pagedListConfig()).build()
    private val latestReleaseFilmsPagingState: LiveData<PagingState>? = Transformations.switchMap(latestReleaseDataSource.source) { it.getNetworkState() }

    override fun topFilms() = topFilms
    override fun latestReleases() = latestReleaseFilms

    private fun pagedListConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(10)
        .setEnablePlaceholders(false)
        .setPageSize(10 * 2)
        .build()
}