package com.znevs.filmoteca.feature.film_detail.presentation.ui.detail

import androidx.lifecycle.ViewModel
import com.znevs.core.di.ScreenScope
import com.znevs.core.di.ViewModelFactory
import com.znevs.core.di.ViewModelKey
import com.znevs.core.network.api.ApiService
import com.znevs.filmoteca.DI
import com.znevs.filmoteca.feature.film_detail.data.repository.FilmDetailRepositoryImpl
import com.znevs.filmoteca.feature.film_detail.domain.interceptors.FilmDetailInterceptor
import com.znevs.filmoteca.feature.film_detail.domain.interceptors.FilmDetailInterceptorImpl
import com.znevs.filmoteca.feature.film_detail.domain.repository.FilmDetailRepository
import com.znevs.filmoteca.feature.film_detail.presentation.viewmodel.detail.FilmDetailViewModel
import com.znevs.filmoteca.util.ResourceProvider
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap


@Component(modules = [FilmDetailModule::class])
@ScreenScope
interface FilmDetailComponent {

    fun viewModelFactory(): ViewModelFactory

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun resources(resourceProvider: ResourceProvider): Builder

        @BindsInstance
        fun api(apiService: ApiService): Builder

        fun build(): FilmDetailComponent
    }

    companion object {
        fun create() = with(DI.appComponent) {
            DaggerFilmDetailComponent.builder()
                .resources(resources())
                .api(DI.networkComponent.api())
                .build()
        }
    }
}

@Module
abstract class FilmDetailModule {

    @Binds
    @IntoMap
    @ViewModelKey(FilmDetailViewModel::class)
    abstract fun filmModelViewModel(viewModel: FilmDetailViewModel): ViewModel

    @Binds
    @ScreenScope
    abstract fun filmDetailRepository(repository: FilmDetailRepositoryImpl): FilmDetailRepository

    @Binds
    @ScreenScope
    abstract fun filmDetailInteractor(interactor: FilmDetailInterceptorImpl): FilmDetailInterceptor
}

