plugins {
    id(GradlePluginId.ANDROID_APPLICATION)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    id(GradlePluginId.KOTLIN_KAPT)
    id(GradlePluginId.SAFE_ARGS)
}

apply<plugins.ApplicationPlugin>()

androidExtensions {
    isExperimental = true
}

dependencies {

    // Adapter
    implementation(LibraryDependency.ADAPTER_DELEGATES)
    implementation(LibraryDependency.ADAPTER_LAYOUT_CONTAINER)
    implementation(LibraryDependency.ADAPTER_PAGINATION)

    // Dagger
    implementation(LibraryDependency.DAGGER)
    kapt(LibraryDependency.DAGGER_COMPILER)
}
