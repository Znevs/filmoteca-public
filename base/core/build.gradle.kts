plugins {
    id(GradlePluginId.ANDROID_LIBRARY)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    id(GradlePluginId.KOTLIN_KAPT)
}

apply<plugins.LibraryPlugin>()

dependencies {
    api(LibraryDependency.KOTLIN_STD_LIB)

    // Android
    api(LibraryDependency.CORE_KTX)
    api(LibraryDependency.APP_COMPAT)

    // Navigation
    api(LibraryDependency.NAVIGATION_FRAGMENT)
    api(LibraryDependency.NAVIGATION_UI)
    api(LibraryDependency.NAVIGATION_DYNAMIC_FEATURE_FRAGMENT_KTX)

    // UI
    api(LibraryDependency.CONSTRAINT_LAYOUT)
    api(LibraryDependency.FRAGMENT_KTX)
    api(LibraryDependency.MATERIAL_DESIGN)

    // Coil
    api(LibraryDependency.COIL)

    // ReduxKotlin
    api(LibraryDependency.REDUX)

    // Timber
    api(LibraryDependency.TIMBER)

    // Dagger
    api(LibraryDependency.DAGGER)
    kapt(LibraryDependency.DAGGER_COMPILER)

}