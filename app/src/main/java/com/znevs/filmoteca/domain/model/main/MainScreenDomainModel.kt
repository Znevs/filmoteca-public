package com.znevs.filmoteca.domain.model.main

data class MainScreenDomainModel(
    val topFilms: FilmsHorizontalItem?,
    val latestReleasesFilms: FilmsHorizontalItem?
)