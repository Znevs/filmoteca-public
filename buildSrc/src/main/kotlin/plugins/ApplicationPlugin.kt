package plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import plugins.configs.configureAndroidApplication
import plugins.configs.configureTestDependencies
import plugins.configs.configureCoreDependencies

open class ApplicationPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.configureAndroidApplication()
        project.configureCoreDependencies()
        project.configureTestDependencies()
    }
}