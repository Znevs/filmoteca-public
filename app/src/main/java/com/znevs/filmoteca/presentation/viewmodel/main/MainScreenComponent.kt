package com.znevs.filmoteca.presentation.viewmodel.main

import androidx.lifecycle.ViewModel
import com.znevs.core.di.ScreenScope
import com.znevs.core.di.ViewModelFactory
import com.znevs.core.di.ViewModelKey
import com.znevs.core.network.api.ApiService
import com.znevs.filmoteca.DI
import com.znevs.filmoteca.data.repository.FilmRepositoryImpl
import com.znevs.filmoteca.domain.interceptors.MainScreenInteractor
import com.znevs.filmoteca.domain.interceptors.MainScreenInteractorImpl
import com.znevs.filmoteca.domain.repository.FilmRepository
import com.znevs.filmoteca.util.ResourceProvider
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap

@Component(modules = [MainScreenModule::class])
@ScreenScope
interface MainScreenComponent {

    fun viewModelFactory(): ViewModelFactory

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun resources(resourceProvider: ResourceProvider): Builder

        @BindsInstance
        fun api(apiService: ApiService): Builder

        fun build(): MainScreenComponent
    }

    companion object {
        fun create() = with(DI.appComponent) {
            DaggerMainScreenComponent.builder()
                .resources(resources())
                .api(DI.networkComponent.api())
                .build()
        }
    }
}

@Module
abstract class MainScreenModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel::class)
    abstract fun mainScreenViewModel(viewModel: MainScreenViewModel): ViewModel

    @Binds
    @ScreenScope
    abstract fun filmRepository(repository: FilmRepositoryImpl): FilmRepository

    @Binds
    @ScreenScope
    abstract fun mainInteractor(interactor: MainScreenInteractorImpl): MainScreenInteractor
}

