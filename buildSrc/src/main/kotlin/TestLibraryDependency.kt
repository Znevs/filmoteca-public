private object TestLibraryVersion {
    const val JUNIT = "4.13"
    const val MOCKITO = "3.3.3"
    const val ANDROID_X_TEST = "1.1.0"
    const val ESPRESSO = "3.1.0"
    const val KAKAO = "2.3.2"
    const val FRAGMENT = "1.2.4"
    const val KOTLIN_COROUTINES = "1.3.4"
}

object TestLibraryDependency {
    const val JUNIT = "junit:junit:${TestLibraryVersion.JUNIT}"
    const val ESPRESSO = "androidx.test.espresso:espresso-core:${TestLibraryVersion.ESPRESSO}"
    const val KAKAO = "com.agoda.kakao:kakao:${TestLibraryVersion.KAKAO}"
    const val ANDROID_X_TEST_EXT = "androidx.test.ext:junit-ktx:${TestLibraryVersion.ANDROID_X_TEST}"
    const val ANDROID_X_TEST_RUNNER = "androidx.test:runner:${TestLibraryVersion.ANDROID_X_TEST}"
    const val ANDROID_X_TEST_RULES = "androidx.test:rules:${TestLibraryVersion.ANDROID_X_TEST}"
    const val ANDROID_X_TEST_CORE = "androidx.test:core:${TestLibraryVersion.ANDROID_X_TEST}"
    const val MOCKITO = "org.mockito:mockito-core:${TestLibraryVersion.MOCKITO}"
    const val MOCKITO_ANDROID = "org.mockito:mockito-android:${TestLibraryVersion.MOCKITO}"
    const val FRAGMENT = "androidx.fragment:fragment-testing:${TestLibraryVersion.FRAGMENT}"
    const val KOTLIN_COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${TestLibraryVersion.KOTLIN_COROUTINES}"
}
