package com.znevs.core.domain.model.base

interface ListItem {
    val itemId: Long
}