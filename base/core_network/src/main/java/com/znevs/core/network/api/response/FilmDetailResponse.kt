package com.znevs.core.network.api.response

import com.znevs.core.network.model.FilmDetailDataModel

data class FilmDetailResponse(
    val data: FilmDetailDataModel
)