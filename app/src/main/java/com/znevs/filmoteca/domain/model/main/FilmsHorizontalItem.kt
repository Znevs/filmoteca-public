package com.znevs.filmoteca.domain.model.main

import androidx.paging.PagedList
import com.znevs.core.domain.model.base.ListItem

data class FilmsHorizontalItem(
    val title: String,
    val films: PagedList<ListItem>?
) : ListItem {
    override val itemId: Long = title.hashCode().toLong()
}