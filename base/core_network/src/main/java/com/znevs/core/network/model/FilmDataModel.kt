package com.znevs.core.network.model

data class FilmDataModel(
    val filmId: Long,
    val nameRu: String?,
    val nameEn: String?,
    val year: String?,
    val rating: Double?,
    val posterUrl: String?
)
