package com.znevs.filmoteca.feature.film_detail.presentation.viewmodel.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.znevs.core.network.model.base.ResultWrapper
import com.znevs.core.presentation.viewmodel.base.BaseViewModel
import com.znevs.filmoteca.feature.film_detail.domain.interceptors.FilmDetailInterceptor
import com.znevs.filmoteca.feature.film_detail.domain.model.FilmDetailDomainModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class FilmDetailViewModel @Inject constructor(
    private val interceptor: FilmDetailInterceptor
) : BaseViewModel() {

    private val _data = MutableLiveData<FilmDetailDomainModel>()
    val data: LiveData<FilmDetailDomainModel> = _data

    fun setArgs(filmId: Long) {
        loadFilmDetail(filmId)
    }

    private fun loadFilmDetail(filmId: Long) {
        viewModelScope.launch {
            when (val result = interceptor.loadFilmDetail(filmId)) {
                is ResultWrapper.Success -> {
                    _data.postValue(result.value)
                }
            }
        }
    }
}