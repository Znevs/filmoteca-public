package plugins.configs

import extensions.addTestDependencies
import extensions.addCoreDependencies
import extensions.addAppDependencies
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

internal fun Project.configureTestDependencies() = dependencies {
    addTestDependencies()
}

internal fun Project.configureCoreDependencies() = dependencies {
    addCoreDependencies()
}

internal fun Project.configureAppDependencies() = dependencies {
    addAppDependencies()
}