package com.znevs.core.presentation.ui.base

import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment {
    constructor(contentLayoutId: Int) : super(contentLayoutId)
    constructor() : super()
}