import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id(GradlePluginId.ANDROID_LIBRARY)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    id(GradlePluginId.KOTLIN_KAPT)
}

apply<plugins.LibraryPlugin>()

android {
    defaultConfig {
        val key: String = gradleLocalProperties(rootDir).getProperty("apiKey")
        buildConfigField("String", "API_KEY", "\"$key\"")
    }
}

dependencies {
    // Dagger
    api(LibraryDependency.DAGGER)
    kapt(LibraryDependency.DAGGER_COMPILER)

    // Network
    implementation(LibraryDependency.RETROFIT)
    implementation(LibraryDependency.OKHTTP_LOGGING_INTERCEPTOR)
    implementation(LibraryDependency.MOSHI_CONVERTER)
}