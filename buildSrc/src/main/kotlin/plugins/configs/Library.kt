package plugins.configs

import configs.AndroidConfig
import configs.BuildType
import configs.BuildTypeDebug
import configs.BuildTypeRelease
import com.android.build.gradle.BaseExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

private typealias LibraryBaseExtension = BaseExtension

internal fun Project.configureAndroidLibrary() = this.extensions.getByType<LibraryBaseExtension>().run {
    compileSdkVersion(AndroidConfig.COMPILE_SDK_VERSION)

    defaultConfig {
        minSdkVersion(AndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(AndroidConfig.TARGET_SDK_VERSION)
        testInstrumentationRunner = AndroidConfig.TEST_INSTRUMENTATION_RUNNER
    }

    signingConfigs {
        setProperty("archivesBaseName", AndroidConfig.ANDROID_BUILD_NAME)
    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            buildConfigField("String", "API_URL", BuildTypeRelease.apiUrl)

            isDebuggable = BuildTypeRelease.isDebuggable
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled

            proguardFiles("proguard-android.txt", "proguard-rules.pro")
        }

        getByName(BuildType.DEBUG) {
            buildConfigField("String", "API_URL", BuildTypeDebug.apiUrl)

            isDebuggable = BuildTypeDebug.isDebuggable
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled

            proguardFiles("proguard-android.txt", "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    project.tasks.withType(KotlinCompile::class.java).configureEach {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    useLibrary("android.test.runner")
    useLibrary("android.test.base")
    useLibrary("android.test.mock")
}