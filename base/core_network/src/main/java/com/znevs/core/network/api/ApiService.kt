package com.znevs.core.network.api

import com.znevs.core.network.api.response.FilmDetailResponse
import com.znevs.core.network.api.response.LatestReleaseFilmsResponse
import com.znevs.core.network.api.response.TopFilmsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("/api/v2.1/films/top")
    suspend fun getTopFilms(
        @Query("page") page: Int = 1
    ): TopFilmsResponse?

    @GET("/api/v2.1/films/releases")
    suspend fun getLatestReleasesFilms(
        @Query("page") page: Int = 1,
        @Query("year") year: Int = 2020,
        @Query("month") month: String = "JUNE"
    ): LatestReleaseFilmsResponse?

    @GET("/api/v2.1/films/{id}")
    suspend fun getFilmDetail(
        @Path("id") filmId: Long
    ): FilmDetailResponse?
}