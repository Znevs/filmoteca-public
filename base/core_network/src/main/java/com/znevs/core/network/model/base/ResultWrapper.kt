package com.znevs.core.network.model.base

sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    data class Error(val t: Throwable) : ResultWrapper<Nothing>()
}