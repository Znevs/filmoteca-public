package com.znevs.filmoteca.presentation.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.znevs.core.presentation.ui.base.BaseFragment
import com.znevs.filmoteca.R
import com.znevs.filmoteca.domain.model.main.FilmsHorizontalItem
import com.znevs.filmoteca.presentation.ui.main.adapters.MainScreenAdapter
import com.znevs.filmoteca.presentation.viewmodel.main.MainScreenComponent
import com.znevs.filmoteca.presentation.viewmodel.main.MainScreenViewModel
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment(R.layout.fragment_main) {

    private val component by lazy { MainScreenComponent.create() }
    private val viewModel by viewModels<MainScreenViewModel> { component.viewModelFactory() }

    private val adapter by lazy {
        MainScreenAdapter {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToFeatureFilmDetailNavGraph(it.id))
        }
    }

    private lateinit var itemList: MutableList<FilmsHorizontalItem>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemList = mutableListOf(
            FilmsHorizontalItem(
                getString(R.string.top_films),
                null
            ),
            FilmsHorizontalItem(
                getString(R.string.latest_releases),
                null
            )
        )

        mainScreenRecyclerView.adapter = adapter
        adapter.items = itemList

        configureObservables()
    }

    private fun configureObservables() {
        viewModel.data.observe(viewLifecycleOwner, Observer {
            itemList[0] = FilmsHorizontalItem(
                it.topFilms?.title ?: "data1",
                it.topFilms?.films
            )
            itemList[1] = FilmsHorizontalItem(
                it.latestReleasesFilms?.title ?: "data2",
                it.latestReleasesFilms?.films
            )
            adapter.notifyDataSetChanged()
        })
    }
}