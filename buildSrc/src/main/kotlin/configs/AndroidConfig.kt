package configs

import java.time.LocalDate
import java.time.format.DateTimeFormatter

object AndroidConfig {
    const val MIN_SDK_VERSION = 23
    const val TARGET_SDK_VERSION = 29
    const val COMPILE_SDK_VERSION = 29
    const val BUILD_TOOLS_VERSION = "29.0.3"

    const val APPLICATION_NAME = "Filmoteca"
    const val APPLICATION_ID = "com.znevs.filmoteca"
    const val TEST_INSTRUMENTATION_RUNNER = "androidx.test.runner.AndroidJUnitRunner"

    val ANDROID_BUILD_NAME = "$APPLICATION_NAME-${LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))}-v${AndroidVersionConfig.VERSION_NAME}"
}

object AndroidVersionConfig {
    private const val MAJOR_VERSION = 0
    private const val MINOR_VERSION = 0
    private const val BUILD_VERSION = 1

    const val VERSION_NAME = "$MAJOR_VERSION.$MINOR_VERSION.$BUILD_VERSION"
    const val VERSION_CODE = MAJOR_VERSION * 10_000 + MINOR_VERSION * 100 + BUILD_VERSION
}

interface BuildType {

    companion object {
        const val RELEASE = "release"
        const val DEBUG = "debug"
    }

    val apiUrl: String
    val isDebuggable: Boolean
    val isMinifyEnabled: Boolean
    val isShrinkResources: Boolean
    val applicationIdSuffix: String
    val versionNameSuffix: String
    val appName: String
}

object BuildTypeDebug : BuildType {
    override val apiUrl = "\"https://kinopoiskapiunofficial.tech\""
    override val isDebuggable = true
    override val isMinifyEnabled = false
    override val isShrinkResources = false
    override val applicationIdSuffix = ".debug"
    override val versionNameSuffix = "-debug"
    override val appName = "${AndroidConfig.APPLICATION_NAME} (debug)"
}

object BuildTypeRelease : BuildType {
    override val apiUrl = "\"https://kinopoiskapiunofficial.tech\""
    override val isDebuggable = false
    override val isMinifyEnabled = true
    override val isShrinkResources = true
    override val applicationIdSuffix = ""
    override val versionNameSuffix = "-release"
    override val appName = AndroidConfig.APPLICATION_NAME
}