package plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import plugins.configs.configureAndroidFeature
import plugins.configs.configureAppDependencies
import plugins.configs.configureCoreDependencies
import plugins.configs.configureTestDependencies

open class FeaturePlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.configureAndroidFeature()
        project.configureAppDependencies()
        project.configureCoreDependencies()
        project.configureTestDependencies()
    }
}