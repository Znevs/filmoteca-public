package plugins.configs

import com.android.build.gradle.internal.dsl.BaseAppModuleExtension
import configs.*
import extensions.ModuleDependency
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

private typealias ApplicationBaseExtension = BaseAppModuleExtension

internal fun Project.configureAndroidApplication() = this.extensions.getByType<ApplicationBaseExtension>().run {
    compileSdkVersion(AndroidConfig.COMPILE_SDK_VERSION)

    defaultConfig {
        applicationId = AndroidConfig.APPLICATION_ID
        minSdkVersion(AndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(AndroidConfig.TARGET_SDK_VERSION)
        buildToolsVersion(AndroidConfig.BUILD_TOOLS_VERSION)

        versionName = AndroidVersionConfig.VERSION_NAME
        versionCode = AndroidVersionConfig.VERSION_CODE
        testInstrumentationRunner = AndroidConfig.TEST_INSTRUMENTATION_RUNNER
    }

    signingConfigs {
        setProperty("archivesBaseName", AndroidConfig.ANDROID_BUILD_NAME)
    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            buildConfigField("String", "API_URL", BuildTypeRelease.apiUrl)

            isDebuggable = BuildTypeRelease.isDebuggable
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled

            proguardFiles("proguard-android.txt", "proguard-rules.pro")

            applicationIdSuffix = BuildTypeRelease.applicationIdSuffix
            versionNameSuffix = BuildTypeRelease.versionNameSuffix
            resValue("string", "app_name", BuildTypeRelease.appName)
        }

        getByName(BuildType.DEBUG) {
            buildConfigField("String", "API_URL", BuildTypeDebug.apiUrl)

            isDebuggable = BuildTypeDebug.isDebuggable
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled

            proguardFiles("proguard-android.txt", "proguard-rules.pro")

            applicationIdSuffix = BuildTypeDebug.applicationIdSuffix
            versionNameSuffix = BuildTypeDebug.versionNameSuffix
            resValue("string", "app_name", BuildTypeDebug.appName)
        }
    }

    dynamicFeatures = ModuleDependency.getDynamicFeatureModules().toMutableSet()

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    project.tasks.withType(KotlinCompile::class.java).configureEach {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    useLibrary("android.test.runner")
    useLibrary("android.test.base")
    useLibrary("android.test.mock")
}