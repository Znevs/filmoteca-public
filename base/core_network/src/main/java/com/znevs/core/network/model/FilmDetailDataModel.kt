package com.znevs.core.network.model

data class FilmDetailDataModel(
    val filmId: Long,
    val nameRu: String?,
    val nameEn: String?,
    val posterUrl: String?,
    val year: String?,
    val description: String?
)
