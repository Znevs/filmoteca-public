package com.znevs.filmoteca.feature.film_detail.domain.model

data class FilmDetailDomainModel(
    val filmId: Long,
    val nameRu: String,
    val nameEn: String,
    val posterUrl: String,
    val year: String,
    val description: String
)
