package com.znevs.core.network.api.base

import com.znevs.core.network.model.base.ResultWrapper
import retrofit2.HttpException

suspend fun <T> safeApiCall(apiCall: suspend () -> T): ResultWrapper<T> {
    return try {
        ResultWrapper.Success(apiCall.invoke())
    } catch (throwable: Throwable) {
        ResultWrapper.Error(throwable)
    }
}

private fun convertErrorBody(throwable: HttpException): String? {
    return try {
        throwable.response()?.errorBody()?.toString()
    } catch (exception: Exception) {
        null
    }
}