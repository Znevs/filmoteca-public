package plugins

import plugins.configs.configureTestDependencies
import org.gradle.api.Plugin
import org.gradle.api.Project
import plugins.configs.configureAndroidLibrary

open class LibraryPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.configureAndroidLibrary()
        project.configureTestDependencies()
    }
}