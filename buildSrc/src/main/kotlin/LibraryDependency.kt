private object LibraryVersion {
    const val DAGGER = "2.28.1"
    const val ADAPTER = "4.3.0"
    const val NAVIGATION = "2.3.0"
}

object LibraryDependency {
    const val KOTLIN_STD_LIB = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${CoreVersion.KOTLIN}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.7"

    // Android
    const val CORE_KTX = "androidx.core:core-ktx:1.2.0"
    const val APP_COMPAT = "androidx.appcompat:appcompat:1.1.0"

    // Navigation
    const val NAVIGATION_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${LibraryVersion.NAVIGATION}"
    const val NAVIGATION_UI = "androidx.navigation:navigation-ui-ktx:${LibraryVersion.NAVIGATION}"
    const val NAVIGATION_DYNAMIC_FEATURE_FRAGMENT_KTX = "androidx.navigation:navigation-dynamic-features-fragment:${LibraryVersion.NAVIGATION}"

    // UI
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:1.1.3"
    const val MATERIAL_DESIGN = "com.google.android.material:material:1.1.0"
    const val FRAGMENT_KTX = "androidx.fragment:fragment-ktx:1.2.5"

    // Coil
    const val COIL = "io.coil-kt:coil:0.11.0"

    // Dagger
    const val DAGGER = "com.google.dagger:dagger:${LibraryVersion.DAGGER}"
    const val DAGGER_COMPILER = "com.google.dagger:dagger-compiler:${LibraryVersion.DAGGER}"

    // Adapter
    const val ADAPTER_DELEGATES = "com.hannesdorfmann:adapterdelegates4-kotlin-dsl:${LibraryVersion.ADAPTER}"
    const val ADAPTER_LAYOUT_CONTAINER = "com.hannesdorfmann:adapterdelegates4-kotlin-dsl-layoutcontainer:${LibraryVersion.ADAPTER}"
    const val ADAPTER_PAGINATION = "com.hannesdorfmann:adapterdelegates4-pagination:${LibraryVersion.ADAPTER}"

    // Network
    const val RETROFIT = "com.squareup.retrofit2:retrofit:2.9.0"
    const val OKHTTP_LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:4.7.2"
    const val MOSHI_CONVERTER = "com.squareup.retrofit2:converter-moshi:2.9.0"

    // ReduxKotlin
    const val REDUX = "org.reduxkotlin:redux-kotlin-threadsafe-jvm:0.5.1"

    // Timber
    const val TIMBER = "com.jakewharton.timber:timber:4.7.1"
}