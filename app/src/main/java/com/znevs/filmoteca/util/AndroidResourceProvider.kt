package com.znevs.filmoteca.util

import android.content.Context
import com.znevs.filmoteca.util.ResourceProvider
import javax.inject.Inject

class AndroidResourceProvider @Inject constructor(
  private val context: Context
)  : ResourceProvider {

  override fun string(id: Int): String = context.resources.getString(id)

}