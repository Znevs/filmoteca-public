package com.znevs.filmoteca.domain.model.main

import com.znevs.core.domain.model.base.ListItem

object ProgressFilmItem : ListItem {
    override val itemId: Long = 0
}