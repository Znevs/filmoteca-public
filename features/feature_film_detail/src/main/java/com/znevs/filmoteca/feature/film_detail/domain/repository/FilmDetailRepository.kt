package com.znevs.filmoteca.feature.film_detail.domain.repository

import com.znevs.core.network.model.FilmDetailDataModel
import com.znevs.core.network.model.base.ResultWrapper

interface FilmDetailRepository {
    suspend fun loadFilmDetail(filmId: Long): ResultWrapper<FilmDetailDataModel?>
}